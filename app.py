from flask import Flask, render_template, abort, request
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import update

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)

class TimestampedModel(db.Model):
    __abstract__ = True

    created_at = db.Column(db.DateTime, default=db.func.now())
    updated_at = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

class Stock(db.Model):
    __tablename__ = 'Stock'
    id = db.Column(db.Integer, primary_key=True)
    article = db.Column(db.String(255))
    ref = db.Column(db.String(255))
    quantity = db.Column(db.Integer)

    def __repr__(self):
        return '<Stock : "{}">'.format(self.article)

@app.route('/')
def home():
    return render_template('pages/home.html')

@app.route('/liste')
def articleList():
    list = Stock.query.all()
    return render_template('pages/product-list.html', list=list)

@app.route('/ajout-référence')
def newArticle():
    return render_template('pages/new-article.html')

@app.route('/ajout-référence', methods=['POST'])
def addArticle():
    a = Stock()
    a.article = request.form['article']
    a.ref = request.form['ref']
    a.quantity = request.form['quantity']
    db.session.add(a)
    db.session.commit()
    return render_template('pages/new-article.html')

@app.route('/liste', methods=['POST'])
def updateQuantity():
    a = Stock.query.get(int(request.form['id']))
    a.quantity = request.form['quantity']
    a.id = request.form['id']
    update(Stock).where(Stock.id==a.id).values(quantity=a.quantity)
    # db.session.add(new)
    db.session.commit()
    return articleList()


@app.route('/stock', methods=['POST'])
def deleteArticle():
    ref = request.form['article']
    a = Stock.query.get(int(id))
    db.session.delete(a)
    db.session.commit()
    return render_template('pages/product-list.html')
#
# @app.errorhandler(404)
# def page_not_found(error):
#     return render_template('errors/404.html'), 404

if __name__ == '__main__':
    db.create_all()
    app.run(debug=True, host='localhost', port=5000)
